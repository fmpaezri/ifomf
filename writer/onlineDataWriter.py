# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 22:27:25 2013

@author: Jorge Vanegas
"""

import threading
import logging 
try:
    import Queue as queue
except ImportError:
    # Python 3
    import queue

class OnlineDataWriter:
    """
    """

    minibatch_index = 0
    
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        
    def start_writer(self, online_writer):
        """Star reader thread
        """
        
        self.online_writer = online_writer
        self.minibatch_index = 0
        self.online_writer._continue = True
        self.thread = threading.Thread(target=online_writer.run)
        self.thread.start()
        
    def join(self):
        self.thread.join()
        
        
    def stop_writer(self):
        self.online_writer._continue = False
    
    def write_minibatch(self, minibatch):
        """Returns the next multimodal minibatch
        """
        
        self.logger.debug("adding minibatch %s...", self.minibatch_index )
        self.minibatch_index += 1
        minibatch = self.online_writer.minibatch_queue.put(minibatch)
        return minibatch
        
    def close(self):
        """Release datasource
        """
        
        self.online_writer.close()


class OnlineWriter:
    """
    """
    minibatch_index = 0
    _continue = True
    
    def __init__(self):
        self.logger = logging.getLogger(__name__)
    
    def run(self):
        """Reader main loop
        """
        
        self.minibatch_queue = None
        self.minibatch_queue = queue.Queue()
        self.minibatch_index = 0
        while self._continue:
            if not self.minibatch_queue.empty():
                self.logger.debug('writing minibatch in index ' + str(self.minibatch_index))
                self.write_next_minibatch(self.minibatch_queue.get())
        self.close()
        
    def write_next_minibatch(self, minibatch):
        """Add the next multimodal minibatch to the queue 
        """

        self.write_minibatch(self.minibatch_index, minibatch.T)
        self.minibatch_index += minibatch.shape[1]       
        
    def write_minibatch(self, minibatch_index, minibatch):
        """Write the i-th multimodal minibatch
        
        :param int minibatch_index: position index in dataset for the multimodal minibatch
        """
        
        raise NotImplementedError()
        
    
    def close(self):
        """release datasource
        """
        raise NotImplementedError()
        