# -*- coding: utf-8 -*-
"""
Created on Tue Oct 29 11:24:39 2013

@author: Jorge Vanegas
"""

import h5py
import numpy as np
import logging 
from onlineDataWriter import OnlineWriter

class HDF5OnlineWriter(OnlineWriter):
    """secuencial writer implemenatation for HDF5 multimodal datasets             
    """
    
    def __init__(self, file_name, dataset_path, shape):
        
        """construct a new HDF5OnlineReader instance.
        
        :param int minibatch: minibatch size
        :param str file_name: valid file name
        :param dataset_paths: strings with paths for each modality 
        """
        
        self.logger = logging.getLogger(__name__)
        self.h5_file = h5py.File(file_name, 'w')   
        self.dataset = self.h5_file.create_dataset(dataset_path ,shape, maxshape=(None,shape[1]))
        
    def write_minibatch(self, minibatch_index, minibatch):
        self.dataset[minibatch_index:minibatch_index + minibatch.shape[0],:] = minibatch
        
    def close(self):
        """close the hdf5 file
        """
        
        self.h5_file.close()
        
        
        
        
        
        
        
    
    
