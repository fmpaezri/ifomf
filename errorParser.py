import re
import sys

config = re.compile(r'python generateRepresentations.py.+results/R_.+_\d+_\d+_(.+)_(.+)__(.+)_.+_(.+)_\d+.h5')
#init = re.compile(r'Initial cost: (.+)')
nCost = re.compile(r' cost: (.+)')
error = re.compile(r'rror\[(.+)\]')
end = re.compile(r'Elapsed time')

def parse(path):
    log = open(path)
    costs = {}
    errors = {}
    configFound = False
    configs = None
    for i in log:
        if not configFound:
            configs = re.findall(config, i)
            if len(configs) > 0:
                configFound = True
                costs[configs[0]] = []
                errors[configs[0]] = []
        else:
            ended = re.findall(end, i)
            if len(ended) > 0:
                configFound = 0
                configs = None
                continue
            cost = re.findall(nCost, i)
            if len(cost) > 0:
                costs[configs[0]].append(float(cost[0]))
                continue
            es = re.findall(error, i)
            if len(es) > 0:
                errors[configs[0]].append(map(float,es[0].split(', ')))
                continue
    
    return (costs, errors)
