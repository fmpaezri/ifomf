# -*- coding: utf-8 -*-
"""
Created on Fri Nov  8 14:33:21 2013

@author: Jorge Vanegas
"""
import numpy as np

class LearningRule:
    
    """Class which computes new parameter values
    given (1) a learning rate (2) current parameter values and (3) the current
    estimated gradient.
    """
    
    def update(self, g, x):
        pass

class SimpleDecreasingStep(LearningRule):
    """Implementation of LearningRule using a simple decreasing rule for the 
    learning rate
    """
    
    def __init__(self, gamma0=0.001, _lambda=0.001):
        """Construct a new SimpleDecreasingStep instance.
        
        :param float gamma0: initial step size decay
        :param float _lambda: regularization parameter 
        """
        
        self.gamma0 = gamma0
        self._lambda = _lambda
        self.iteration = 0
    
    def update(self, g, x):
        self.gamma = self.gamma0/(1 + self.gamma0*self._lambda*self.iteration); 
        self.iteration += 1
        return x - self.gamma * g
    
    
class AdaDelta(LearningRule):
    """Implements the AdaDelta learning rule as described in:
    "AdaDelta: An Adaptive Learning Rate Method", Matthew D. Zeiler.
    """   
    
    decay = 0.95
    epsilon = 0.00001
    mean_square_grad = None
    mean_square_dx = None
    
    def __init__(self, decay=0.95, epsilon=0.00001):
        """Construct a new AdaDelta instance.

        :param float decay: decay rate \rho in Algorithm 1 of the 
        afore-mentioned paper.
        """
            
        assert decay >= 0.
        assert decay < 1.
        self.decay = decay
        self.epsilon = epsilon
    
    def update(self, g, x):
        """ 
        """
        
        if self.mean_square_dx is None:
            self.mean_square_dx = np.asmatrix(np.zeros(x.shape))
            self.mean_square_grad = np.asmatrix(np.zeros(g.shape))
        
        # Accumulate gradient
        # mean_squared_grad := E[g^2]_{t-1}
        new_mean_squared_grad = self.decay * self.mean_square_grad + (1 - self.decay) * np.power(g,2)

        # Compute update
        # mean_square_dx := E[(\Delta x)^2]_{t-1}
        rms_dx_tm1 = np.sqrt(self.mean_square_dx + self.epsilon)
        rms_grad_t = np.sqrt(new_mean_squared_grad + self.epsilon)
        delta_x_t = - np.multiply(rms_dx_tm1 / rms_grad_t, g)
        
        # Accumulate updates
        new_mean_square_dx = self.decay * self.mean_square_dx + (1 - self.decay) * np.power(delta_x_t,2)
        # Apply update
        self.mean_square_dx = new_mean_square_dx
        self.mean_square_grad = new_mean_squared_grad
        
        return x + delta_x_t
        
