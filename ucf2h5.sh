ds=ucf101
ds_name=~/datasets/$ds/hdf5/$ds.h5
m1_name=/ds/visual
m1_path=~/repos/iccv2013/features/mbh.dense.matrix
m2_name=/ds/textual
m2_path=~/repos/iccv2013/features/textual.dense.matrix
m3_name=/ds/attributes
m3_path=~/repos/iccv2013/features/attributes.dense.matrix
m4_name=/ds/devel_indices
m4_path=~/repos/iccv2013/ucfTrainTestlist/devel01.idx
m5_name=/ds/eval_indices
m5_path=~/repos/iccv2013/ucfTrainTestlist/eval01.idx
#m5_path=~/repos/iccv2013/test.idx
m6_name=/ds/queries_indices
m6_path=~/repos/iccv2013/ucfTrainTestlist/test01.idx
#m6_path=~/repos/iccv2013/test.idx
m7_name=/ds/database_indices
m7_path=~/repos/iccv2013/ucfTrainTestlist/train01.idx
if [ ! -d ~/datasets/$ds/hdf5 ];
	then mkdir ~/datasets/$ds/hdf5
fi
python plain2h5.py $ds_name $m1_name $m1_path $m2_name $m2_path $m3_name $m3_path $m4_name $m4_path $m5_name $m5_path $m6_name $m6_path
