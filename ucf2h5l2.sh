ds=ucf101
ds_name=~/datasets/$ds/hdf5/$ds.l2.h5
m1_name=/ds/mbh
m1_path=~/repos/iccv2013/features/mbh.dense.l2.matrix
m2_name=/ds/textual
m2_path=~/repos/iccv2013/features/textual.dense.matrix
m3_name=/ds/attributes
m3_path=~/repos/iccv2013/features/attributes.dense.l2.matrix
m4_name=/ds/devel_indices
m4_path=~/repos/iccv2013/ucfTrainTestlist/devel01.idx
m5_name=/ds/eval_indices
m5_path=~/repos/iccv2013/ucfTrainTestlist/eval01.idx
#m5_path=~/repos/iccv2013/test.idx
m6_name=/ds/queries_indices
m6_path=~/repos/iccv2013/ucfTrainTestlist/test01.idx
#m6_path=~/repos/iccv2013/test.idx
m7_name=/ds/database_indices
m7_path=~/repos/iccv2013/ucfTrainTestlist/train01.idx
m8_name=/ds/hog
m8_path=~/repos/iccv2013/features/hog.dense.l2.matrix
m9_name=/ds/hof
m9_path=~/repos/iccv2013/features/hof.dense.l2.matrix
m10_name=/ds/tr
m10_path=~/repos/iccv2013/features/tr.dense.l2.matrix
if [ ! -d ~/datasets/$ds/hdf5 ];
	then mkdir ~/datasets/$ds/hdf5
fi
python plain2h5.py $ds_name $m1_name $m1_path $m2_name $m2_path $m3_name $m3_path $m4_name $m4_path $m5_name $m5_path $m6_name $m6_path $m7_name $m7_path $m8_name $m8_path $m9_name $m9_path $m10_name $m10_path
