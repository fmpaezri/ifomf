dataset=ucf101
generator=generateRepresentations.py
ranker=~/nsebp/nmfScripts/nmfCoding_hdf5.py
inputPath=$(dataset).l2.h5
datasetPaths=/ds/mbh,/ds/hof,/ds/hog,/ds/tr,/ds/attributes,/ds/textual
trainIdxPath=/ds/devel_indices
minibatch=256
weights=0.08,0.05,0.05,0.02,0.6,0.2
epochs=4
factors=1500
experiment:=simple.dtf.trimodal.l2
representationsPath=representation.$(experiment).$(dataset).h5
rankingPath=ranking.$(experiment).$(dataset)
resultsPath=results.$(experiment).$(dataset)
hPath=/ds/H
uPath=/ds/U
pPath=/ds/P
testIdxPath=/ds/eval_indices
projectedModalityIndex=0,1,2,3
calculateError=True
measure=dp
xi=0.01
lambda=0.001
gamma=0.0001
learningRule=simple
profileOutput=omf.prof
utilsDir=~/utils
trecEval=$(utilsDir)/trecvid.tools/trec_eval_video/trec_eval
trecMaxResults=20000

#Default task by position
$(representationsPath) generateRepresentations: $(inputPath)
	time python $(generator) $(inputPath) $(datasetPaths) $(trainIdxPath) $(minibatch) $(weights) $(epochs) $(factors) $(representationsPath) $(hPath) $(uPath) $(pPath) $(testIdxPath) $(projectedModalityIndex) $(learningRule) $(calculateError) $(xi) $(lambda) $(gamma)

$(rankingPath) rank: $(representationsPath)
	time python $(ranker) $^ $(hPath) $(uPath) $(databaseList) $(queriesList) $(measure) > $@

$(resultsPath): $(rankingPath)
	time $(trecEval) -q $(qrelsFile) $^ $(trecMaxResults) > $@

clean:
	rm $(representationsPath) $(rankingPath) $(resultsPath)

#Weights to explore
modalWeights:=0.08,0.05,0.05,0.02,0.6,0.2
#Factors to explore
factorList:=500 1000 1500 2000
#Lambda to explore
lambdas:=0.0001 0.001
#minibatch to explore
minibatches:=256
#epochs to explore
epochList:=4
gammas:=0.0001 0.001
runs:=1
#experiment:=simple.textual.l2

#Definition as comma cant appear on functions by itself as it is the arg separator
comma:=,
encodeParams=$(foreach i,$(2),$(1)_$(i))
decodeParams=$(subst _,=,$(1))
encodeFilename=evaluate/$(dataset)/$(experiment)/eval_$(learningRule)_$(minibatch)_$(epochs)_$(lambda)_$(gamma)_$(alpha)_$(subst $(comma),_,$(weights))_$(factors)

#Recursive call setting factors, target and outputfile
explore:
	$(MAKE) $(call encodeFilename)_$(run).txt

sample: $(call encodeParams,run,$(shell seq $(runs)))
	echo "Running $(call encodeFilename)" 

#"grep -e 'map' -e 'P10 ' $(foreach run,$(shell seq $(runs)),$(call encodeFilename)_$(run).txt) > $(call encodeFilename).txt

run%:
	$(MAKE) explore $(call decodeParams,$@)

factors%:
	$(MAKE) sample $(call decodeParams,$@)

weights%: 
	$(MAKE) $(call encodeParams,factors,$(factorList)) $(call decodeParams,$@)

lambda%: 
	$(MAKE) $(call encodeParams,weights,$(modalWeights)) $(call decodeParams,$@)

minibatch%: 
	$(MAKE) $(call encodeParams,lambda,$(lambdas)) $(call decodeParams,$@)

epochs%: 
	$(MAKE) $(call encodeParams,minibatch,$(minibatches)) $(call decodeParams,$@)

gamma%: 
	$(MAKE) $(call encodeParams,epochs,$(epochList)) $(call decodeParams,$@)

#Named task to generate the full sweep
parameterSweep: 
	$(MAKE) $(call encodeParams,gamma,$(gammas))

#Implicit recipe for result file
results/R%.h5:
	$(MAKE) generateRepresentations representationsPath=$@

testEncodeParams:
	echo $(call encodeParams,hola,10 20 30)

testDecodeParams:
	echo $(call decodeParams,gamma,gamma=10)

databaseList=~/nsebp/datasets/$(dataset)/devel_list.txt
queriesList=~/nsebp/datasets/$(dataset)/eval_list.txt
qrelsFile=~/nsebp/datasets/$(dataset)/qrels_$(dataset).txt.eval

ranking/rank%.txt: results/R%.h5
	time python $(ranker) $^ $(hPath) $(uPath) $(databaseList) $(queriesList) $(measure) > $@

evaluate/$(dataset)/$(experiment):
	mkdir -p $@

evaluate/$(dataset)/$(experiment)/eval%.txt: ranking/rank%.txt | evaluate/$(dataset)/$(experiment)
	time $(trecEval) $(qrelsFile) $^ $(trecMaxResults) > $@

######################################
#           EXPERIMENTAL
######################################

modalityFiles:=database.txt.matrix queries.txt.matrix
fullModality:=/tmp/modality.full
#databaseList=database_list.txt
#queriesList=queries_list.txt
trainIdx=train.idx
testIdx=test.idx

concatenateModality:
	cat $(modalityFiles) > $(fullModality)

generateIndexes:
	dbSize=$$(wc -l $(databaseList) | awk '{print $$1}') && qSize=$$(wc -l $(queriesList) | awk '{print $$1}') && for i in $$(seq 0 $$((dbSize-1))); do echo $$i >> $(trainIdx); done && for i in $$(seq $$dbSize $$((dbSize+qSize-1))); do echo $$i >> $(testIdx); done

normalize:
	python normalize.py $(unnormalizedInput) $(norm) $(normalizedOutput) $(scale)
