# -*- coding: utf-8 -*-
"""
Created on Fri Nov  8 12:38:34 2013

@author: Jorge Vanegas
"""
import logging

from reader import *
from writer import *
import numpy as np
from sklearn import preprocessing
import numpy.matlib
#import cumat

class OMF:
    """Implements the Online Multimodal Matrix Factorization as described in:
    "Online Matrix Factorization for Multimodal Image Retrieval", Juan C. 
    Caicedo and Fabio A. González
    """
    
    learning_rule = None
    reader = None
    data_provider = None
    factors = 0
    shapes = []
    modality_indices = []
    # multimodal transformation matrix
    P = None
    # full dataset, just for testing
    X = None
    logger = None
    compute_error = False
    error = []
    testError = []
    
    def __init__(self, learning_rule, reader, compute_error=False, projectedModalities=None, xi=0.01):
        """Construct a new OMF instance.
        
        :param OnlineDataProvider reader
        :param LearningRule learning_rule 
        """
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        self.learning_rule = learning_rule
        self.compute_error = compute_error
        self.data_provider
        self.reader = reader
        self.projectedModalities = projectedModalities
        self.xi = xi
        
    def joint_learning(self, epochs_number, factors, _lambda):
        """
        
        :param int epochs_number: numbre of epochs
        :param int factors: number of latent factors
        :param float _lambda: regularization parameter
        """
        self.initialization(factors)
        self._lambda = _lambda
        self.data_provider = OnlineDataProvider()
        
        epoch = 0        
        while epoch < epochs_number:
            
            self.data_provider.start_reader(self.reader)
            self.logger.info("epoch: " + str(epoch))
            iteration = 0
            while self.data_provider.has_more_Data():
                self.next_iteration()
                iteration += 1
            if self.compute_error:
                self.error.append(self.calculate_error(self._lambda, self.X, self.P, self.error, self.projectedModalities))
                self.testError.append(self.calculate_error(self._lambda, self.Xt, self.P, self.testError, self.projectedModalities))
                cost = self.cost(self._lambda, self.X, self.P, self.latent_representation(self._lambda, self.X, self.P))
                self.logger.info("{0}-th cost: {1}".format(epoch, cost))
                #for i in range(5):
                #    self.gradTest(self._lambda, self.X, self.P)
            epoch += 1
        #self.data_provider.close()
        self.data_provider.join()
        return self.P, self.error
        
    def next_iteration(self):
        # draw xk
        x = self.data_provider.get_next_minibatch()   
        # compute latent factor representation
        self.h = self.latent_representation(self._lambda, x, self.P)            
        # compute gradient
        g = self.gradient(self._lambda, x, self.P, self.h) 
        # update multimodal transformation matrix
        self.P = self.learning_rule.update(g, self.P)
        #if self.compute_error:
        #    self.logger.info("H stats")
        #    self.stats(self.h)
        #    self.logger.info("P stats")
        #    self.stats(self.P)
        return self.P, self.h 
            
    def calculate_error(self, _lambda, X, P, error, projectedModalities=None):
        self.logger.debug("calculating error..")
        H = self.joint_coding(self.slice_modalities(X, projectedModalities), projectedModalities, _lambda, self.xi)
        start = 0
        i = 0
        for stop in self.modality_indices:
            error[i].append(np.linalg.norm(X[start:stop,:] - P[start:stop,:]*H)**2)
            start = stop
            i += 1
        return error

    def stats(self, P):
        #minimum = np.min(P)
        #maximum = np.max(P)
        norm = np.linalg.norm(P)
        #mean = np.mean(P)
        #std = np.std(P)
        self.logger.info("Matrix statistics: norm {0}".format(norm))

    def cost(self, _lambda, X, P, H):
        delta = X-P*H
        Pnorm = np.linalg.norm(P)
        Hnorm = np.linalg.norm(H)
        regularization = _lambda*(np.power(Pnorm,2) + np.power(Hnorm,2))
        cost = np.power(np.linalg.norm(np.sqrt(self.w)*delta.A),2)+regularization
        start = 0
        i = 0
        for shape in self.shapes:
            error = delta[start:start + shape[1], :]
            self.logger.info("Reconstruction error for modality {0}:\tF:{1}\tL1:{2}\tP:{3}".format( i, np.linalg.norm(error), np.sum(np.abs(error)), np.linalg.norm(P[start:start + shape[1], :])))
            i += 1
            start += shape[1]
        self.logger.info("Regularization: P:{0}\tH:{1}".format(Pnorm, Hnorm))
        return cost

    def gradTest(self, _lambda, X, P):
        sample = X[:, np.random.randint(X.shape[1])]
        h = self.latent_representation(_lambda, sample, P)
        cost = self.cost(_lambda, sample, P, h)
        grad = self.gradient(_lambda, sample, P, h)
        deltas = [0.001, 0.0001, 0.00001, 0.000001]
        delta = deltas[np.random.randint(len(deltas))]
        Pp = P + delta
        hP = self.latent_representation(_lambda, sample, Pp)
        perturbedCost = self.cost(_lambda, sample, Pp, hP)
        difference = perturbedCost - cost
        self.logger.info("Difference for delta {0}: {1}".format(delta, difference))

    def gradient(self, _lambda, x, P, h):
        return (self.w * (P*h - x).A) *h.T + _lambda*P
        
    def latent_representation(self, _lambda, x, P):
        return np.linalg.inv(_lambda*np.eye(self.factors) + P.T*(self.w * P.A))*P.T*(self.w * x)

    def initialization(self, factors):
        self.factors = factors
        self.shapes = self.reader.get_shapes()
        modality_index = 0
        self.w = np.zeros((np.sum([y for (x,y) in self.shapes]), 1))
        i = 0
        for shape in self.shapes:
            modality_index += shape[1]
            self.modality_indices.append(modality_index)
            self.w[modality_index-shape[1]:modality_index] = self.reader.weights[i]
            i += 1

        #print "W1:", self.w
        #is this really doing something?
        self.w = self.w.reshape(modality_index,1)
        #print "W2:", self.w

        self.P = np.asmatrix(preprocessing.normalize(np.random.random((self.reader.full_dimension, self.factors)), norm='l1'))
        self.logger.debug("P shape:" + str(self.P.shape))
        
        if self.compute_error:
            self.X = self.reader.read()
            self.logger.debug("X shape {0}".format(self.X.shape))
            cost = self.cost(0.0, self.X, np.matlib.zeros(self.P.shape), np.zeros((self.P.shape[1], self.X.shape[1])))
            self.logger.info("Initial cost: {0}".format(cost))
            self.Xt = self.reader.indexed_read(self.reader.test_indices)
            self.logger.debug("Xt shape {0}".format(self.Xt.shape))
            for shape in self.shapes:
                self.error.append([])
                self.testError.append([])
                
    def write_latent_representation(self, writer):
        self.logger.info("Writing the latent representation for training data...");
        self.reader.shuffle = False
        self.data_provider.start_reader(self.reader)
        
        data_writer = OnlineDataWriter()
        data_writer.start_writer(writer)
        while self.data_provider.has_more_Data():
            x = self.data_provider.get_next_minibatch()
            h = self.latent_representation(x, self.P)
            data_writer.write_minibatch(h)
        data_writer.stop_writer()
        data_writer.join()
        
    def joint_coding(self, X, modalities, _lambda=0.000001, xi=0.0):
        '''
        :param X: known modality data
        :param float xi: regularization parameter
        :param int modality: modality index 
        '''

        xi_vector = []
        i = 0
        for shape in self.shapes:
            mXi = 1.0 if i in modalities else xi
            xi_vector.extend([mXi for k in range(shape[1])])
            i += 1
        XI = np.diag(xi_vector)

        PP = self.P.T*XI*self.P

        Pslice = self.slice_modalities(self.P, modalities)
        self.logger.info("Pslice[{0}]\tX[{1}]".format(Pslice.shape, X.shape))
        U =  np.linalg.inv(PP + _lambda*np.eye(self.factors))*(Pslice.T*X)

        return U

    def slice_modalities(self, X, modalities):
        slices = None
        for modality in modalities:
            start = self.modality_indices[modality]-self.shapes[modality][1]
            stop = self.modality_indices[modality]
            if slices == None:
                slices = np.arange(start,stop)
            else:
                slices = np.hstack([slices, np.arange(start,stop)])
        return X[slices,:]

    def modal_representation(self, H, outputModality=None):
        output = self.P*H
        if outputModality == None:
            return output
        else:
            start = self.modality_indices[outputModality]-self.shapes[outputModality][1]
            stop = self.modality_indices[outputModality]
            return output[start:stop,:]
