import sys
from sklearn import preprocessing
import numpy as np

def help():
    print "Usage: ", sys.argv[0], " inputFile norm outputFile scale"

if len(sys.argv) < 5:
    help()
    sys.exit(1)

inputFile = sys.argv[1]
norm = sys.argv[2]
outputFile = sys.argv[3]
scale = float(sys.argv[4])

lines = open(inputFile)
output = open(outputFile, 'w')

array = []
for line in lines:
    vector = [map(float,line.replace('\n', '').strip().split())]
    normalizedVector = preprocessing.normalize(vector, norm=norm)
    outputLine = ' '.join(map(str, normalizedVector[0] * scale)) + '\n'
    output.write(outputLine)

output.close()
