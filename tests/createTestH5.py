# -*- coding: utf-8 -*-
"""
Created on Tue Oct 29 17:59:10 2013

@author: Jorge Vanegas
"""

import h5py
import numpy as np

dataset_name = 'test.h5'
f = h5py.File(dataset_name, 'w')
g = f.create_group('/ds')
v_dim = 5
t_dim = 5
samples = 10
visual = f.create_dataset('/ds/visual', (samples,v_dim), maxshape=(None,v_dim))
textual = g.create_dataset('textual', (samples,t_dim), maxshape=(None,t_dim))
train_indices = g.create_dataset('train_indices', (5,1), maxshape=(None,None))
test_indices = g.create_dataset('test_indices', (5,1), maxshape=(None,None))

visual[...] = np.eye(samples,v_dim)
visual[5:10,:] = np.eye(5,v_dim)
textual[0:samples,:] = np.eye(samples,t_dim)
textual[samples:samples+samples,:] = np.eye(samples,t_dim)
train_indices[:,0] =  np.arange(0,5).T
test_indices[:,0] =  np.arange(5,10).T

f.close()

#h5dump
