# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 20:10:02 2013

@author: Jorge Vanegas
"""

try:
    import Queue as queue
except ImportError:
    # Python 3
    import queue

import logging
import threading
import random
import time

class ProduceToQueue( threading.Thread ):
   def __init__( self, threadName, queue ):
      threading.Thread.__init__( self, name = threadName )
      self.sharedObject = queue
      
   def run( self ):
      for i in range(1, 2):
         time.sleep( random.randrange( 3 ) )
         logger.debug("%s adding %s to queue", self.getName(), i )
         self.sharedObject.put( i )

      logger.debug("finished producing values")
      logger.debug("Terminating" + self.getName())

class ConsumeFromQueue( threading.Thread ):
   def __init__( self, threadName, queue ):
      threading.Thread.__init__( self, name = threadName )
      self.sharedObject = queue

   def run( self ):
      sum = 0
      current = 10

      for i in range( 2 ):
         time.sleep( random.randrange( 2 ) )
         logger.debug("%s attempting to read %s...", self.getName(), current + 1 )
         current = self.sharedObject.get()
         logger.debug("%s read %s", self.getName(), current )
         sum += current

      logger.debug("%s retrieved values totaling: %d", self.getName(), sum )
      logger.debug("Terminating" + self.getName())

logging.basicConfig(level=logging.DEBUG) #logging.WARNING | logging.INFO | logging.DEBUG
logger = logging.getLogger(__name__)
 

'''
# create a file handler
handler = logging.FileHandler('hello.log')
handler.setLevel(logging.INFO)

# create a logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(handler)
'''

queue = queue.Queue()
producer = ProduceToQueue( "Producer", queue )
consumer = ConsumeFromQueue( "Consumer", queue )

producer.start()
consumer.start()

producer.join()
consumer.join()