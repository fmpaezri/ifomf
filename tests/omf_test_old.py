# -*- coding: utf-8 -*-
"""
Created on Thu Nov 14 15:40:10 2013

@author: Jorge Vanegas
"""

from reader import *
from omf import *
from learningRule import *

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

file_name = '/Users/macbook/Workspace/hdf5/test.h5'

# use [h5dump -H corel5k.h5] for review the file structure use
visual = '/training/visual'
textual = '/training/textual'

shuffle = False
minibatch_size = 4
full = True

reader = HDF5OnlineReader(file_name, minibatch_size, shuffle, full, visual, textual)

#print reader.read()

_lambda = 1
epochs_number = 1
factors = 4
calculate_error=True

learningRule = AdaDelta()

omf = OMF(learningRule, reader, calculate_error)
[P, H] = omf.joint_learning(epochs_number, factors, _lambda)

print(H)
print(P)

logger.info(omf.error)