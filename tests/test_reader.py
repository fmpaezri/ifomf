# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 23:48:24 2013

@author: macbook
"""

from reader import *

logging.basicConfig(level=logging.DEBUG)

file_name = '/Users/macbook/Workspace/hdf5/test.h5'

# use [h5dump -H corel5k.h5] for review the file structure use
visual = '/training/visual'
textual = '/training/textual'

shuffle = False
minibatch_size = 4
full = False

reader = HDF5OnlineReader(file_name, minibatch_size, shuffle, full, visual, textual)

dataProvider = OnlineDataProvider()
dataProvider.start_reader(reader)

while(dataProvider.has_more_Data()):
    print dataProvider.get_next_minibatch()
