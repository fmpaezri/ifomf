# -*- coding: utf-8 -*-
"""
Created on Thu Nov 14 15:40:10 2013

@author: Jorge Vanegas
"""

from reader import *
from omf import *
from learningRule import *
import numpy as np

logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

file_name = 'test.h5'

# use [h5dump -H corel5k.h5] for review the file structure use
visual = '/ds/visual'
textual = '/ds/textual'
indices_path = '/ds/train_indices'

shuffle = False
minibatch_size = 11
full = True
modalities = [visual, textual]
weights = [0.5, 0.5]

reader = HDF5OnlineReader(file_name, minibatch_size, shuffle, full, modalities, weights, indices_path)

_lambda = 0.64423
gamma0 = np.power(2.0, -6)
epochs_number = 5
factors = 4
calculate_error=True

#learning_rule = AdaDelta()
learning_rule = SimpleDecreasingStep(gamma0, _lambda)

omf = OMF(learning_rule, reader, calculate_error)
[P, error] = omf.joint_learning(epochs_number, factors, _lambda)

logger.info('error' + str(omf.error[0]))
logger.info('error' + str(omf.error[1]))

results_file_name = 'results.h5'
h_path = '/ds/H'
h_shape = [reader.samples, factors]
writer = HDF5OnlineWriter(results_file_name, h_path, h_shape)

omf.write_latent_representation(writer)

p_path = '/ds/P'
h5_file = h5py.File(results_file_name, 'a') 
p_dataset = h5_file.create_dataset(p_path,P.shape, maxshape=(None,P.shape[1]))
p_dataset[...] = P
h5_file.close()

h5_file = openFile(file_name, 'r')
test_indices_path = '/ds/test_indices'
test_indices = h5_file.getNode(test_indices_path)[...][:,0]

test_indices_path = '/ds/visual'
X = h5_file.getNode(test_indices_path)[test_indices]
xi = 0.01
modality = 0
U = omf.joint_coding(X, xi, modality)
U = U.T
u_path = '/ds/U'
h5_file = h5py.File(results_file_name, 'a') 
u_dataset = h5_file.create_dataset(u_path,U.shape, maxshape=(None,U.shape[1]))
u_dataset[...] = U
h5_file.close()



