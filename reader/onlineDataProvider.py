# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 22:27:25 2013

@author: Jorge Vanegas
"""
import numpy as np
import random
import threading
import logging 
try:
    import Queue as queue
except ImportError:
    # Python 3
    import queue

class OnlineDataProvider:
    """
    """

    minibatch_index = -1
    minibatch_number = -1
    online_reader = None
    
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        
    def start_reader(self, online_reader):
        """Star reader thread
        """
        
        self.online_reader = online_reader
        self.minibatch_index = -1
        self.minibatch_number = online_reader.get_minibatch_number()
        self.thread = threading.Thread(target=online_reader.run)
        self.thread.start()
        
        
    def join(self):
        self.thread.join()
        
    def has_more_Data(self):
        """Return True if there are more minibatches in the queue
        """        
        self.logger.debug(str(self.minibatch_index + 1) + ':' + str(self.minibatch_number))
        return self.minibatch_index + 1 < self.minibatch_number
    
    def get_next_minibatch(self):
        """Returns the next multimodal minibatch
        """
        self.minibatch_index += 1
        self.logger.debug("attempting to read minibatch %s...", self.minibatch_index )
        minibatch = self.online_reader.minibatch_queue.get()
        self.logger.debug("read minibatch %s", self.minibatch_index)
        return minibatch
        
    def close(self):
        """release datasource
        """
        self.online_reader.close()


class OnlineReader:
    """
    """
    indices = []
    indices_path = None
    minibatch_index = -1
    minibatch_queue = None
    
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.minibatch_queue = None
        self.minibatch_queue = queue.Queue()
    
    def run(self):
        """Reader main loop
        """
        
        self.generate_indices()
        self.minibatch_index = -1
        while self.has_more_Data():
            self.minibatch_queue.put(self.read_next_minibatch())
        
    def read_next_minibatch(self):
        """Add the next multimodal minibatch to the queue 
        """
        
        self.minibatch_index += 1
        return self.read_minibatch(self.minibatch_index, self.indices, self.train_indices)
        
    def has_more_Data(self):
        """Return True if there are more minibatches to read
        """
        
        if self.minibatch_index + 1 < self.minibatch_number:
            return True
        return False
        
    def generate_indices(self):
        """Generates a list of random indices"""
        
        self.indices = np.arange(self.samples)
        if self.shuffle:
            random.shuffle(self.indices)
            
    def load_indices(self, indices_path):
        raise NotImplementedError()
        
        
    def read_minibatch(self, minibatch_index, indices):
        """return the i-th multimodal minibatch from the dataset
        
        :param int minibatch_index: index of required  multimodal minibatch
        :param list indices: list of indices
        :return: list of matrices, one per modality
        """
        raise NotImplementedError()
        
    def get_shapes(self):
        """return a list of shapes for each dataset
        """
        raise NotImplementedError()
        
    def get_minibatch_number(self):
        """return the number of minibatches
        """
        raise NotImplementedError()
    
    def close(self):
        """release datasource
        """
        raise NotImplementedError()
        
    def read(self):
        """read full dataset
        """
        raise NotImplementedError()
        
