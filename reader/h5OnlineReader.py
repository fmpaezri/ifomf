# -*- coding: utf-8 -*-
"""
Created on Tue Oct 29 11:24:39 2013

@author: Jorge Vanegas
"""
import logging
from tables import *
import numpy as np
from onlineDataProvider import OnlineReader

class HDF5OnlineReader(OnlineReader):
    """Secuencial reader implemenatation for HDF5 multimodal datasets             
    """
    minibatch_size = 0
    minibatch_number = -1
    file_name = None
    dataset_paths = []
    h5_file = None
    datasets = []
    shapes = [] 
    samples = -1
    shuffle = False
    last_minibatch_size = 0
    logger = None
    full = False
    full_dimension = 0
    
    def __init__(self, file_name, minibatch_size, shuffle, full, dataset_paths, weights, indices_path, test_indices_path):
        
        """Construct a new HDF5OnlineReader instance.
        
        :param int minibatch: minibatch size
        :param str file_name: valid file name
        :param dataset_paths: strings with paths for each modality 
        """
        
        OnlineReader.__init__(self)
        self.minibatch_size = minibatch_size
        self.file_name = file_name
        self.dataset_paths = dataset_paths
        self.shuffle = shuffle
        self.full = full
        self.indices_path = indices_path
        self.h5_file = openFile(file_name, 'r')
        self.load_train_indices(indices_path)
        self.load_test_indices(test_indices_path)
        
        if len(weights) != len(dataset_paths):
            raise Exception('numbers of modalities doesn\'t match')
        self.weights = weights
        
        for dp in dataset_paths:
            self.logger.debug("dataset: " + dp)
            dataset = self.h5_file.getNode(dp)
            if self.samples == -1:
                self.samples = self.train_indices.shape[0]
                self.minibatch_number = self.samples / self.minibatch_size
                self.last_minibatch_size = self.samples % self.minibatch_size
                if (self.last_minibatch_size != 0):
                    self.minibatch_number += 1
            self.full_dimension += dataset.shape[1]
            self.datasets.append(dataset)    
            self.shapes.append(dataset.shape)
        
    def read_minibatch(self, minibatch_index, indices, train_indices):
        """Add the i-th multimodal minibatch to the queue
        
        :param int minibatch_index: index of required  multimodal minibatch
        :return: list of matrices, one per modality
        """
        
        start_index = minibatch_index * self.minibatch_size
        stop_index = start_index + self.minibatch_size
        self.logger.debug("Reading shuffle index range {0}".format((start_index, stop_index)))
        if self.full:
            
            modality = np.zeros((self.get_minibatch_size(stop_index), 
                                     self.full_dimension,), dtype=float)
            dim = 0
            d_index = 0
            for dataset in self.datasets:
                next_dim = dim + dataset.shape[1]
                modality[:, dim : next_dim] = dataset[train_indices[indices[start_index:stop_index],0], :]
                dim = next_dim 
                d_index += 1                       
            return modality.T
        else:
            modalities = []
            d_index = 0
            for dataset in self.datasets:
                modality = np.zeros((self.get_minibatch_size(stop_index), 
                                     dataset.shape[1],), dtype=float)
                modality = dataset[train_indices[indices[start_index:stop_index]], :]
                modalities.append(modality.T)
                d_index += 1
            return modalities
        
    def get_minibatch_size(self, stop_index):
        """return the next minibatch taking into account the number 
        of remaining examples
        """
        if stop_index <= self.samples:
            return self.minibatch_size
        return self.last_minibatch_size
        
    def load_train_indices(self, indices_path):
        self.train_indices = self.load_node(indices_path)

    def load_test_indices(self, indices_path):
        self.test_indices = self.load_node(indices_path)
    
    def load_node(self, nodePath):
        return self.h5_file.getNode(nodePath)[...]    
        
    def get_minibatch_number(self):
        self.logger.debug("minibatch number: " + str(self.minibatch_number))
        return self.minibatch_number
            
    def get_shapes(self):
        self.logger.debug("shapes: " + str(self.shapes))
        return self.shapes
        
    def close(self):
        self.h5_file.close()
        
    def read(self):
        return self.indexed_read(self.train_indices)

    def indexed_read(self, index):
        #if index == None:
        #    index = np.array(np.arange(self.samples)).reshape(self.samples,1)
        if self.full:
            modality = np.zeros((index.shape[0], self.full_dimension,), dtype=float)
            dim = 0
            for dataset in self.datasets:
                next_dim = dim + dataset.shape[1]
                modality[:, dim : next_dim] = dataset[index[:,0],:]
                self.logger.info("Dataset {0} F-norm {1}".format(dataset.name, np.linalg.norm(modality[:, dim : next_dim])))
                dim = next_dim
            return modality.T
        else:
            modalities = []
            for dataset in self.datasets:
                modality = dataset[index[:,0],:]
                modalities.append(np.asmatrix(modality.T))
            return modalities
