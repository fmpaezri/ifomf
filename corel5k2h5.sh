ds=corel5k
ds_name=~/nsebp/datasets/$ds/hdf5/$ds.h5.norm
m1_name=/ds/visual
m1_path=/tmp/visual
m2_name=/ds/textual
m2_path=/tmp/textual
m3_name=/ds/train_indices
m3_path=~/nsebp/datasets/$ds/train.idx
m4_name=/ds/test_indices
m4_path=~/nsebp/datasets/$ds/test.idx
if [ ! -d ~/nsebp/datasets/$ds/hdf5 ];
	then mkdir ~/nsebp/datasets/$ds/hdf5
fi
python plain2h5.py $ds_name $m1_name $m1_path $m2_name $m2_path $m3_name $m3_path $m4_name $m4_path
