# -*- coding: utf-8 -*-
"""
Created on Thu Nov 14 15:40:10 2013

@author: Jorge Vanegas
"""
import logging

logging.basicConfig(level=logging.INFO)

from reader import *
from omf import *
from learningRule import *
import numpy as np
import h5py
import sys
from timeit import default_timer as timer

def help():
    print "Usage:", sys.argv[0], "inputPath", "datasetPath1,datasetPath2,...", "trainIdxPath", "minibatch", "weight1,...", "epochs", "factors", \
    "outputPath", "hPath", "uPath", "pPath", "testIdxPath", "projectedModalities", "learningMethod(ada|simple)", "[calculateError]", "[xi]", "[lambda]", "[gamma]", "[alpha]"

if len(sys.argv) < 15:
    help()
    sys.exit(1)

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

file_name = sys.argv[1]

# use [h5dump -H corel5k.h5] for review the file structure use
datasetPaths = sys.argv[2].split(",")
indices_path = sys.argv[3]

minibatch_size = int(sys.argv[4])
shuffle = False
full = True
modalities = datasetPaths
weights = map(float,sys.argv[5].split(","))
if len(weights) != len(modalities):
    print "Number of weights and modalities do not match"
    sys.exit(1)

epochs_number = int(sys.argv[6])
factors = int(sys.argv[7])
results_file_name = sys.argv[8]
h_path = sys.argv[9]
u_path = sys.argv[10]
p_path = sys.argv[11]
test_indices_path = sys.argv[12]
modalityIndexes = map(int, sys.argv[13].split(","))

xi = 0.01 if len(sys.argv) < 16 else float(sys.argv[16])
calculate_error = False if len(sys.argv) < 15 else sys.argv[15] == 'True'
_lambda = 0.0000001 if len(sys.argv) < 17 else float(sys.argv[17])
gamma0 = 0.0001 if len(sys.argv) < 18 else float(sys.argv[18])

if sys.argv[14] == 'ada':
    learning_rule = AdaDelta()
elif sys.argv[14] == 'simple':
    learning_rule = SimpleDecreasingStep(gamma0, _lambda)
else:
    print "Undefined learning rule", sys.argv[14]
    sys.exit(1)

reader = HDF5OnlineReader(file_name, minibatch_size, shuffle, full, modalities, weights, indices_path, test_indices_path)

omf = OMF(learning_rule, reader, calculate_error, modalityIndexes, xi)
start = timer()
[P, error] = omf.joint_learning(epochs_number, factors, _lambda)
print "Elapsed time", timer()-start

if calculate_error:
    for m in range(len(modalities)):
        logger.info('error' + str(omf.error[m]))
        logger.info('testError' + str(omf.testError[m]))

#h_path = '/H'
h_shape = [reader.samples, factors]
print "H shape", h_shape

modality = np.zeros((reader.samples, reader.full_dimension))
dim = 0
d_index = 0
for dataset in reader.datasets:
    next_dim = dim + dataset.shape[1]
    modality[:, dim : next_dim] = dataset[reader.train_indices[:, 0], :] * reader.weights[d_index]
    dim = next_dim
    d_index += 1
toProject = modality.T

H = omf.latent_representation(_lambda, toProject, P).T

h5_file = h5py.File(results_file_name, 'w')

h_dataset = h5_file.create_dataset(h_path,H.shape, maxshape=(None,H.shape[1]))
h_dataset[...] = H

p_dataset = h5_file.create_dataset(p_path,P.shape, maxshape=(None,P.shape[1]))
p_dataset[...] = P

input_file = openFile(file_name, 'r')
test_indices = input_file.getNode(test_indices_path)[...][:,0]

X = None
for i in modalityIndexes:
    if X == None:
        X = input_file.getNode(modalities[i])[test_indices].T
    else:
        X = np.vstack([X, input_file.getNode(modalities[i])[test_indices].T])
U = omf.joint_coding(X, modalityIndexes, _lambda, xi)
U = U.T
u_dataset = h5_file.create_dataset(u_path,U.shape, maxshape=(None,U.shape[1]))
u_dataset[...] = U
for m in range(len(modalities)):
    Xt = omf.modal_representation(U.T, m).T
    xt_dataset = h5_file.create_dataset(modalities[m], Xt.shape, maxshape=(None,Xt.shape[1]))
    xt_dataset[...] = Xt
h5_file.close()
input_file.close()
