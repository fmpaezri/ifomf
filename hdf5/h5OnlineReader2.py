# -*- coding: utf-8 -*-
"""
Created on Tue Oct 29 11:24:39 2013

@author: Jorge Vanegas
"""

import h5py
import numpy as np
import logging
from OnlineDataProvider import *

class HDF5OnlineReader(OnlineReader):
    """Secuencial reader implemenatation for HDF5 multimodal datasets             
    """
    minibatch_size = 0
    minibatch_number = -1
    file_name = None
    dataset_paths = []
    h5_file = None
    datasets = []
    samples = -1
    shuffle = False
    last_minibatch_size = 0
    logger = None
    
    def __init__(self, file_name, minibatch_size, minibatch_queue, shuffle, *dataset_paths):
        """Construct a new HDF5OnlineReader instance.
        
        :param int minibatch: minibatch size
        :param str file_name: valid file name
        :param dataset_paths: strings with paths for each modality 
        """
        
        self.logger = logging.getLogger(__name__)
        self.minibatch_size = minibatch_size
        self.file_name = file_name
        self.dataset_paths = dataset_paths
        self.shuffle = shuffle
        self.h5_file = h5py.File(file_name, 'r')
        self.minibatch_queue = minibatch_queue

        for dp in dataset_paths:
            self.logger.debug("dataset: " + dp)
            print "dataset: ", dp
            if self.samples == -1:
                self.samples = self.h5_file[dp].shape[0]
                self.minibatch_number = self.samples / self.minibatch_size
                self.last_minibatch_size = self.samples % self.minibatch_size
                if (self.last_minibatch_size != 0):
                    self.minibatch_number += 1
            elif self.h5_file[dp].shape[0] != self.samples:
                raise Exception('Datasets size doesn\'t match')
            self.datasets.append(self.h5_file[dp])    
        
    def read_minibatch(self, minibatch_index, indices):
        """Add the i-th multimodal minibatch to the queue
        
        :param int minibatch_index: index of required  multimodal minibatch
        :return: list of matrices, one per modality
        """
        
        start_index = minibatch_index * self.minibatch_size
        stop_index = start_index + self.minibatch_size
        modalities = []
        for dataset in self.datasets:
            modality = np.zeros((self.get_minibatch_size(stop_index), 
                                 dataset.shape[1],), dtype=float)
            minibatch_indices = indices[start_index:stop_index]
            minibatch_indices.sort()
            modality = dataset[minibatch_indices, :]
            """
            i = 0;
            for index in indices[start_index:stop_index]:
                modality[i,:] = dataset[index,:]
                i += 1
            """
            modalities.append(modality)
        self.minibatch_queue.put(modalities)
        self.logger.debug("Added minibatch %s to the queue", minibatch_index)
        return modalities
        
    def get_minibatch_size(self, stop_index):
        """return the next minibatch taking into account the number 
        of remaining examples
        """
        if stop_index <= self.samples:
            return self.minibatch_size
        return self.last_minibatch_size
        
    def get_minibatch_number(self):
        self.logger.debug("minibatch number: " + self.minibatch_number)
        return self.minibatch_number
        
    def get_shapes(self):
        shapes = [] 
        for dataset in self.datasets:
            shapes.append(dataset.shape)
        self.logger.debug("shapes: " + str(shapes))
        return shapes

        
        
        
        
        
        
    
    
