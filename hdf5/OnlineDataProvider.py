# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 22:27:25 2013

@author: Jorge Vanegas
"""
import numpy as np
import random
import threading
import logging
try:
    import Queue as queue
except ImportError:
    # Python 3
    import queue

class OnlineDataProvider:
    """
    """
    
    minibatch_queue = None
    minibatch_index = -1
    minibatch_number = -1
    online_reader = None
    
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.minibatch_queue = queue.Queue()
        
    def start_reader(self, online_reader):
        """Star reader thread
        """
        self.online_reader = online_reader
        self.minibatch_number = online_reader.get_minibatch_number()
        online_reader.start()
        online_reader.join()
        
    def has_more_Data(self):
        """Return True if there are more minibatches in the queue
        """
        
        if self.minibatch_index + 1 < self.minibatch_number:
            return True
        return False
    
    def get_next_minibatch(self):
        """Returns the next multimodal minibatch
        """
        self.minibatch_number += self.minibatch_number
        self.logger.debug("%s attempting to read %s...", self.getName(), self.minibatch_number )
        minibatch = self.minibatch_queue.get()
        self.logger.debug("%s read %s", self.getName(), self.minibatch_number)
        return minibatch
        
    def read(self):
        """read full dataset
        """
        return self.online_reader.read()


class OnlineReader(threading.Thread):
    """
    """
    indices = []
    minibatch_index = -1
    
    def run(self ):
        """Reader main loop
        """
        self.generate_indices()
        while self.has_more_Data():
            self.minibatch_queue.put(self.read_next_minibatch())
        self.close()
        
    def read_next_minibatch(self):
        """Add the next multimodal minibatch to the queue 
        """
        
        self.minibatch_index += 1
        return self.read_minibatch(self.minibatch_index)
        
    def has_more_Data(self):
        """Return True if there are more minibatches to read
        """
        
        if self.minibatch_index + 1 < self.minibatch_number:
            return True
        return False
        
    def generate_indices(self):
        """Generates a list of random indices"""
        
        self.indices = np.arange(self.samples)
        if self.shuffle:
            random.shuffle(self.indices)
        
    def read_minibatch(self, minibatch_index, indices):
        """return the i-th multimodal minibatch from the dataset
        
        :param int minibatch_index: index of required  multimodal minibatch
        :param list indices: list of indices
        :return: list of matrices, one per modality
        """
        raise NotImplementedError()
        
    def get_shapes(self):
        """return a list of shapes for each dataset
        """
        raise NotImplementedError()
        
    def get_minibatch_number(self):
        """return the number of minibatches
        """
        raise NotImplementedError()
    
    def close(self):
        """release datasource
        """
        raise NotImplementedError()
        
    def read():
        """read full dataset
        """
        raise NotImplementedError()
        
    
    
    
    