import numbapro.cudalib.cublas as cublas
import numpy as np
import numpy.matlib

class Cumat(np.matrix):

    def __mul__(self, other):
        myShape = self.shape
        otherShape = other.shape
        A = np.asmatrix(np.array(self.A, dtype=np.float64, order='F'))
        B = np.asmatrix(np.array(other, dtype=np.float64, order='F'))
        C = np.asmatrix(np.zeros((myShape[0], otherShape[1]), dtype=np.float64, order='F'))
        self.blas = cublas.Blas()
        #print "CUBLAS product {0}X{1}".format(myShape, otherShape)
        self.blas.gemm('N','N', myShape[0], otherShape[1], myShape[1], 1.0, A, B, 0.0, C)
        return np.asmatrix(np.array(C, dtype=C.dtype, order='C')).view(Cumat)
